$(function() {

    Morris.Area({
        element: 'morris-area-chart',
        data: [{
            period: 'Lunes Q1',
            Entregadas: 26,
            Emitidas: 101,
            NoEntregadas: 55
        }, {
            period: 'Martes Q2',
                Entregadas: 15,
                Emitidas: 66,
                NoEntregadas: 6
        }, {
            period: 'Miercoles Q3',
                Entregadas: 55,
                Emitidas: 99,
                NoEntregadas: 2501
        }, {
            period: 'Jueves Q4',
                Entregadas: 21,
                Emitidas: 103,
                NoEntregadas: 77
        }, {
            period: 'Viernes Q1',
                Entregadas: 12,
                Emitidas: 55,
                NoEntregadas: 91
        }],
        xkey: 'period',
        ykeys: ['Entregadas', 'Emitidas', 'NoEntregadas'],
        labels: ['Entregadas', 'Emitidas', 'No Entregadas'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });
    
    
});
